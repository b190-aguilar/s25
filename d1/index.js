console.log("Hello World!");



// SECTION - JSON objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming languages, hence the "notation" in its name.
	- JSON is not to be confused with JavaScript objects
*/


/*
	MINIACTIVITY
		create "cities"	array of objects with the following properties
			city
			province
			country
*/
/*
let cities = [];

class City{
	constructor(city,province,country){
		this.city = city;
		this.province = province;
		this.country = country;
	}
};

let nyc = new City ("New York City",'New York', 'USA');
let mnl = new City ("Manila",'NCR', 'Philippines');
let seoul = new City ("Seoul",'Seoul', 'South Korea');

cities = [nyc, mnl, seoul];
*/

/*
	below is an example of JSON
		when logged in the console, the appearance of JSON does not differ from JS objects. But since JSON is used by other languages as well, it is common to see it as JS object since it is the blueprint of the JSON format.
*/

/*
	WHAT JSON DOES
		- JSON is used for serializing different datatypes into bytes.
		- byte is a unit of data that is composed of eight binary digits (0/1) that is used to  represent a character.
		- serialization is the process of converting data into a series of bytes for easier transmission/transfer of information.

	SYNTAX:
		{
		"propertyA": "valueA",
		"propertyB": "valueB",
		"propertyC": "valueC",
		}
*/

let cities = [
{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
{"city": "Marikina City", "province": "Metro Manila", "country": "Philippines"},
{"city": "Caloocan City", "province": "Metro Manila", "country": "Philippines"},
]

console.log(cities);


// JSON Methods
	/*
		JSON Objects contain methods for parsing and converting data to/from JSON or stringified JSON
	*/


// SECTION - Stringify Method
/*
	- used to convert JS objects into strings.
	- stringified JSON is a JS Object converted into a string (but in JSON format) to be used in other functions of a JS application
	- commonly used in HTTP requests where information is required to be sent and received in a stringified JSON format
	- requests are an important part of programming where an application communicates with a backend application to perform different tasks such as getting/creating data in a database

	- a front application is an app that is used to interact with users to perform different tasks and display information while the backend application are commonly used for all business logic and database processing.

	- a database normally stores information/data that can be used in most applications.
	- commonly stored data in database are user information, transaction records, and product information.
	- NODE and EXPRESS JS are two of technologies used for creating backend applications which process requests from front end applications.
		- Node JS is a Java Runtime Environment (JRE) software that is made to execute other software 
		- Express JS is a Node JS framework that provides features for easily creating web and mobile applications.
*/

let batchesArr = [ {batchName: "Batch X"}, { batchName: "Batch Y"} ];
console.log(batchesArr);


// stringify method
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));


/*
	MINIACTIVITY
		create a JS object
			name: string
			age: number
			address: object{
				city: string
				country: string
			}

		log in the console but make it stringified
*/


let jsObject = {
			name: 'Spongebob Squarepants',
			age: 25,
			address: {
				city: "Bikini Bottom",
				country: "Pacific Ocean"
			},
		}

console.log(JSON.stringify(jsObject));


// direct conversion of JS object to stringified JSON

let data = JSON.stringify({
		name: 'John',
		age: 30,
		address: {
			city: "Batangas",
			country: "Philippines",
		},
	});

console.log(data);


// an example where JSON is commonly used is on package.json file which an express JS application uses to keep track of the information regarding a repository/project (see package.json)




// JSON.stringify with prompt()
/*
	- when information is stored in a variable and is not hard-coded into an object that is being stringified, we can supply the value with a variable
	- the 'property' name and 'value' name would have to be the same and this might be confusing, but this is to signify that the description of the variable and the property name pertains to the same thing and that is the value of the variable itself.

	SYNTAX:
		JSON.stringify({
			propertyA: variableA,
			propertyB: variableB,
			propertyC: variableC,
			});
*/

let fname = "James";
let lname = "Aguilar";
let age = "5";

let address = {
	city: "Bikini Bottom",
	country: "Philippines",
}


let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address,
})

console.log(otherData);



// SECTION - Parse Method
/*
	- converts the stringified JSON into JavaScript objects
	- Objects are common datatypes used in applications because of the complex data strcutures that can be created out of them.
	- information is commonly sent to applications in stringified JSON and then converted back into objects 
*/

let batchesJSON = `[{"batchName":"Batch X"}, {"batchName":"Batch Y"}]`;

console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));



/*
	MINIACTIVITY
		log in the console the parse data coming from the "data" and otherData variables
*/

console.log(JSON.parse(data));
console.log(JSON.parse(otherData));